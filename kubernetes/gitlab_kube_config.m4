apiVersion: v1
clusters:
  - cluster:
      certificate-authority-data: m4KubeCertificateAuthorityData()
      server: https://z2jlrhd8lw.bki1.metakube.syseleven.de:31051
    name: gitlab
contexts:
  - context:
      cluster: gitlab
      namespace: facts
      user: gitlab-admin
    name: gitlab
current-context: gitlab
kind: Config
preferences: {}
users:
  - name: gitlab-admin
    user:
      token: m4KubeToken()
