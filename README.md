# Developers For Future - All ForFuture
Currently it is a Symfony application with some help of Symfony CMF. To run the app locally run a:

```
git clone git@github.com:developersforfuture/website.git
cd website/app/src
composer install
bin/console server:run
```

## Build and Deployment

At the end we do manually build a application wide image by running:

```
cp .env.dist .env # and fill the vars with your values
source .env
# set the version you'd like to tag in ./VERSION
# make
# make push
# that currently does not work properly

# set the same version in kubernetes/app.production.yml
git add .
git commit -m 'set new version on image' 
git tag -s <version-tag-you-want> -m '<message-you-want'
git push --tags origin master

```

### Run the application

There are several ways to run the app.

#### 1 PHP Server by a Symfony command

```bash
cd app/src
composer install
./bin/console server:run
```

### 2 Using Docker-Compose

```bash
source .env
docker-compose up -d
```

## Development

To run it in dev mode it would be the easiest way if you choose the second solution.

### Assets by Webpack

As the assets are handled by webpack, you should run its whatcher when working with Sass or Typescript files. So:

```bash
cd app/src
npm run watch
```
Doing so, you will always get the changes.
